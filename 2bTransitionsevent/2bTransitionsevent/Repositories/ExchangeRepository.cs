﻿using _2bTransitionsevent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _2bTransitionsevent.Repositories
{
    public class ExchangeRepository
    {
        private TwobTransitionseventEntities db = new TwobTransitionseventEntities();

        public IQueryable<Exchange> Query(string storename, string storeID, string serialNumber/*, DateTime ? startdate, DateTime ? enddate*/)
        {
            var query = GetAll();
            //if (startdate.HasValue && DateTime.MinValue != startdate )
            //{
            //    var enddatestr = enddate.HasValue && DateTime.MinValue != enddate ? enddate.Value.AddDays(1) : DateTime.Now;
            //    query = query.Where(p => p.CreateTime >= startdate && p.CreateTime <= enddatestr);

            //}

            if (!string.IsNullOrEmpty(storename))
            {
                query = query.Where(p => p.StoreName.Contains(storename));
            }
            if (!string.IsNullOrEmpty(storeID))
            {
                query = query.Where(p => p.StoreID.Contains(storeID));
            }
            if (!string.IsNullOrEmpty(serialNumber))
            {
                query = query.Where(p => p.SerialNumber == serialNumber);
            }
            return query;
        }
        public IQueryable<Exchange> GetAll()
        {
            var query = db.Exchange;
            return query;
        }
        //public IQueryable<Exchange> FindSerialNumber(string serialNumber)
        //{
        //    var query = GetAll().Where(p => p.SerialNumber == serialNumber);
        //    return query;
        //}
        public IQueryable<Exchange> FindExchangeCode(string code, string serialNumber)
        {
            var query = GetAll().Where(p => p.Code == code || p.SerialNumber == serialNumber);
            return query;
        }

        public int Insert(Exchange newData)
        {
            DateTime now = DateTime.Now;
            newData.CreateTime = now;
            db.Exchange.Add(newData);
            db.SaveChanges();
            return newData.ID;
        }

        public IQueryable<Exchange> FindStoreExchange(string storeID)
        {
            var query = GetAll().Where(p => p.StoreID == storeID);
            return query;
        }

        public Exchange GetById(int id)
        {
            return db.Exchange.Find(id);
        }
    }
}