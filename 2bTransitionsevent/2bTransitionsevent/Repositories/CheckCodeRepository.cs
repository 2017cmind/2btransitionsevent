﻿using _2bTransitionsevent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _2bTransitionsevent.Repositories
{
    public class CheckCodeRepository
    {
        private TwobTransitionseventEntities db = new TwobTransitionseventEntities();

        public IQueryable<CheckCode> Exchanges(string code, string id)
        {
            var query = db.CheckCode.Where(p => p.SerierNumber == code && p.ID == id);
            return query;
        }
    }
}