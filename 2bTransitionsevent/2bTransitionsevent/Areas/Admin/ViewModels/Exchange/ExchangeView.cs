﻿using _2bTransitionsevent.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _2bTransitionsevent.Areas.Admin.ViewModels.Exchange
{
    public class ExchangeView
    {
        private ExchangeRepository exchangeRepository = new ExchangeRepository();

        public int ID { get; set; }

        [Display(Name = "開始日期")]
        public DateTime ? StartDate { get; set; }

        [Display(Name = "結束日期")]
        public DateTime ? EndDate { get; set; }

        [Display(Name = "門市名稱")]
        public string StoreName { get; set; }

        [Display(Name = "門市統編")]
        public string StoreID { get; set; }

        [Display(Name = "門市地址")]
        public string Address { get; set; }

        [Display(Name = "銷售人員")]
        public string Sales { get; set; }

        [Display(Name = "手機")]
        public string Phone { get; set; }

        [Display(Name = "序號")]
        public string SerialNumber { get; set; }

        [Display(Name = "確認碼")]
        public string Code { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateTime { get; set; }

        
        [Display(Name = "總筆數")]
        public int TotalCount { 
            get {
                //var startdate = DateTime.Parse("2021/09/01");
                //var enddate = DateTime.Parse("2021/09/16");
                //var query = exchangeRepository.FindStoreExchange(StoreID);
                //if (StartDate.HasValue && DateTime.MinValue != StartDate)
                //{
                //    var enddatestr = EndDate.HasValue && DateTime.MinValue != EndDate ? EndDate : DateTime.Now;
                //    query = query.Where(p => p.CreateTime >= startdate && p.CreateTime <= enddate);
                //}

                //int count = query.Count();

                int count = exchangeRepository.FindStoreExchange(StoreID).Count();

                return count;
            }
        }
    }
}