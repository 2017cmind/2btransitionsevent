﻿using _2bTransitionsevent.Models.Cmind;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _2bTransitionsevent.Areas.Admin.ViewModels.Exchange
{
    public class ExchangeIndexView : PageQuery
    {
        public ExchangeIndexView()
        {
            this.Sorting = "CreateTime";
            this.IsDescending = true;
        }

        public PageResult<ExchangeView> PageResult { get; set; }

        [Display(Name = "開始日期")]
        public DateTime StartDate { get; set; }

        [Display(Name = "結束日期")]
        public DateTime EndDate { get; set; }

        [Display(Name = "門市統編")]
        public string StoreID { get; set; }

        [Display(Name = "門市名稱")]
        public string StoreName { get; set; }

        [Display(Name = "銷售人員")]
        public string Sales { get; set; }

        [Display(Name = "序號")]
        public string SerialNumber { get; set; }

        [Display(Name = "序號")]
        public string Code { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateTime { get; set; }

        [Display(Name = "總筆數")]
        public string TotalCount { get; set; }
    }
}