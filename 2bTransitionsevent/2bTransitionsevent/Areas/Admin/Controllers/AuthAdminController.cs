﻿using _2bTransitionsevent.ActionFilters;
using _2bTransitionsevent.Areas.Admin.ViewModels.Auth;
using _2bTransitionsevent.Models.Cmind;
using _2bTransitionsevent.Utility.Cmind;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace _2bTransitionsevent.Areas.Admin.Controllers
{
    public class AuthAdminController : BaseAdminController
    {
        // GET: Admin/AuthAdmin
        public ActionResult Login()
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                var controller = DependencyResolver.Current.GetService<AuthAdminController>();
                controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);
                return controller.Login(new LoginView() { Account = "sysadmin", Password = "admin1234" });
            }
#endif
            var login = new LoginView();
            return View(login);
        }

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginView login)
        {
            if (ModelState.IsValid)
            {
                if (login.Account == "sysadmin" && login.Password == "admin1234")
                {
                    AdminInfo adminInfo = new AdminInfo();
                    adminInfo.ID = 1;
                    adminInfo.Account = login.Account;
                    AdminInfoHelper.Login(adminInfo, login.RememberMe);
                    ShowMessage(true, "登入成功");
                    return Redirect(FormsAuthentication.GetRedirectUrl(login.Account, false));
                }
                else
                {
                    ModelState.AddModelError("Password", "帳號或密碼錯誤");
                }
            }

            return View(login);
        }

        [Authorize]
        public ActionResult Logout()
        {
            AdminInfoHelper.Logout();
            return RedirectToAction("Login");
        }
    }
}