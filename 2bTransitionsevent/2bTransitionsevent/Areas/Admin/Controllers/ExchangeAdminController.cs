﻿using _2bTransitionsevent.ActionFilters;
using _2bTransitionsevent.Areas.Admin.ViewModels.Exchange;
using _2bTransitionsevent.Models;
using _2bTransitionsevent.Models.Cmind;
using _2bTransitionsevent.Repositories;
using _2bTransitionsevent.Utility.Cmind;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _2bTransitionsevent.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ExchangeAdminController : BaseAdminController
    {
        private ExchangeRepository exchangeRepository = new ExchangeRepository();
        // GET: Admin/ExchangeAdmin
        public ActionResult Index(ExchangeIndexView model)
        {
            var query = exchangeRepository.Query(model.StoreName, model.StoreID, model.SerialNumber/*, null ,null*/);
            var pageResult = query.ToPageResult<Exchange>(model);
            model.PageResult = Mapper.Map<PageResult<ExchangeView>>(pageResult);
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            ExchangeView model;
            if (id == 0)
            {
                model = new ExchangeView();
            }
            else
            {
                var query = exchangeRepository.GetById(id);
                model = Mapper.Map<ExchangeView>(query);
            }
            return View(model);
        }

        public ActionResult Statistical(ExchangeIndexView model)
        {

            //model.StartDate = DateTime.Parse("2021/09/01");
            //model.EndDate = DateTime.Parse("2021/09/15");
            var query = exchangeRepository.Query(model.StoreName, model.StoreID, null).AsQueryable().GroupBy(x => x.StoreID).Select(x => x.FirstOrDefault());
            //var query = exchangeRepository.Query(model.StoreName, model.StoreID, null, model.StartDate, model.EndDate).AsQueryable().GroupBy(x => x.StoreID).Select(x => x.FirstOrDefault());
            var pageResult = query.ToPageResult2<Exchange>(model);
            model.PageResult = Mapper.Map<PageResult<ExchangeView>>(pageResult);
            model.PageResult.Data = model.PageResult.Data.OrderByDescending(p => p.TotalCount);
            return View(model);
        }
    }
}