﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _2bTransitionsevent.Models.Cmind
{
    public class SiteSetting
    {
        /// <summary>
        /// 網站名稱
        /// </summary>
        public const string SiteName = "2bTEvent";

        /// <summary>
        /// 網站名稱縮寫
        /// </summary>
        public const string SiteNameAbbreviation = "2bTE";

        /// <summary>
        /// 維護商
        /// </summary>
        public const string Maintainer = "Cmind 思脈數位";

        /// <summary>
        /// 維護商網站
        /// </summary>
        public const string MaintainerWebSite = "http://www.cmind.com.tw/";
    }
}