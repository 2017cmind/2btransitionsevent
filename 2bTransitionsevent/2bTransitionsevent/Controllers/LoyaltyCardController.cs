﻿using _2bTransitionsevent.ViewModels.Exchange;
using _2bTransitionsevent.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using _2bTransitionsevent.ActionFilters;

namespace _2bTransitionsevent.Controllers
{
    [ErrorHandleActionFilter]
    public class LoyaltyCardController : BaseController
    {
        private ExchangeRepository exchangeRepository = new ExchangeRepository();
        // GET: LoyaltyCard
        public ActionResult Index(string storeId)
        {
            var model = new ExchangeIndexView();
            if (!string.IsNullOrEmpty(storeId) ||storeId =="Index")
            {
                var query = exchangeRepository.FindStoreExchange(storeId);
                model.PageResult = Mapper.Map<IEnumerable<ExchangeView>>(query);
            }
            else
            {
                RedirectToAction("Index", "Home");
            }
            return View(model);
        }
    }
}