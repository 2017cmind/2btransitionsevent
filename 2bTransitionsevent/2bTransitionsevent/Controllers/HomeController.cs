﻿using _2bTransitionsevent.ViewModels.Exchange;
using _2bTransitionsevent.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using _2bTransitionsevent.Models;
using _2bTransitionsevent.ActionFilters;

namespace _2bTransitionsevent.Controllers
{
    [ErrorHandleActionFilter]
    public class HomeController : BaseController
    {
        private ExchangeRepository exchangeRepository = new ExchangeRepository();
        private CheckCodeRepository checkCodeRepository = new CheckCodeRepository();

        public ActionResult Index()
        {
            var model = new ExchangeView();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ExchangeView model)
        {
            var checkExchangeCode = exchangeRepository.FindExchangeCode(model.Code?.Trim(), model.SerialNumber);
            var checkcode = checkCodeRepository.Exchanges(model.Code?.Trim(), model.SerialNumber);
            bool ischeck = true;
            var message = "" ;
           
            if (!checkcode.Any())
            {
                message = "序號或確認碼輸入錯誤!";
                ischeck = false;
            }

            if (checkExchangeCode.Any())
            {
                message = "此序號或確認碼已被使用";
                ischeck = false;
            }
            if (checkcode.Any())
            {
                if (!checkcode.FirstOrDefault().Status) {
                    message = "此序號與折扣碼已經失效!";
                    ischeck = false;
                }
            }
            if (!ischeck)
            {
                ModelState.AddModelError("Code", message);
                ModelState.AddModelError("SerialNumber", message);
                ShowMessage(false, message);
            }
            if (ModelState.IsValid && ischeck)
            {
                model.Address = model.City + model.Area + model.Address;
                Exchange data = Mapper.Map<Exchange>(model);
                DateTime now = DateTime.Now;
                data.CreateTime = now;
                model.ID = exchangeRepository.Insert(data);
                return RedirectToAction("Index","LoyaltyCard", new { storeId = model.StoreID });
            }
            return View(model);
        }

    }
}