﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _2bTransitionsevent.ViewModels.Exchange
{
    public class ExchangeIndexView
    {
        public IEnumerable<ExchangeView> PageResult { get; set; }
    }
}