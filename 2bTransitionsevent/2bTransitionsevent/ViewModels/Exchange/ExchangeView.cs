﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _2bTransitionsevent.ViewModels.Exchange
{
    public class ExchangeView
    {
        public int ID { get; set; }

        [Display(Name = "門市名稱")]
        public string StoreName { get; set; }

        [Display(Name = "門市統編")]
        public string StoreID { get; set; }

        [Display(Name = "縣市")]
        public string City { get; set; }

        [Display(Name = "區")]
        public string Area { get; set; }

        [Display(Name = "門市地址")]
        public string Address { get; set; }

        [Display(Name = "銷售人員")]
        public string Sales { get; set; }

        [Display(Name = "手機")]
        public string Phone { get; set; }

        [Display(Name = "序號")]
        public string SerialNumber { get; set; }

        [Display(Name = "確認碼")]
        public string Code { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateTime { get; set; }
    }
}