using _2bTransitionsevent.Areas.Admin.Controllers;
using _2bTransitionsevent.Controllers;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace _2bTransitionsevent
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static Logger logger = NLog.LogManager.GetCurrentClassLogger();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //AutoMapper
            App_Start.AutoMapperConfig.Initialize();
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            var httpContext = ((MvcApplication)sender).Context;
            var currentController = " ";
            var currentAction = " ";
            var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(httpContext));

            if (currentRouteData != null)
            {
                if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                {
                    currentController = currentRouteData.Values["controller"].ToString();
                }

                if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                {
                    currentAction = currentRouteData.Values["action"].ToString();
                }
            }

            var ex = Server.GetLastError();
            var routeData = new RouteData();
            var action = "Error";

            if (currentRouteData.DataTokens["area"] == "Admin")
            {
                if (ex is HttpException)
                {
                    var httpEx = ex as HttpException;
                    switch (httpEx.GetHttpCode())
                    {
                        case 404:
                            action = "NotFound";
                            break;
                    }
                }
            }
            httpContext.ClearError();
            httpContext.Response.Clear();
            httpContext.Response.StatusCode = ex is HttpException ? ((HttpException)ex).GetHttpCode() : 500;
            httpContext.Response.TrySkipIisCustomErrors = true;
            routeData.DataTokens["area"] = currentRouteData.DataTokens["area"];
            //����Log
            string msg = string.Format("\r\n�iStackTrace�j:{0}\r\n�iMessage�j:{1}", ex.StackTrace, ex.Message);
            logger.Error(msg);
            routeData.Values["action"] = action;
            if (currentRouteData.DataTokens["area"] == "Admin")
            {
                var adminController = new BaseAdminController();
                routeData.Values["controller"] = "HomeAdmin";
                adminController.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction);
                ((IController)adminController).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
            }
            else
            {
                var frontcontroller = new BaseController();
                routeData.Values["controller"] = "Home";
                frontcontroller.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction);
                ((IController)frontcontroller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
            }
        }
    }
}
