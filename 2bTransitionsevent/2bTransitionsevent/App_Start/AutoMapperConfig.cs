﻿using _2bTransitionsevent.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _2bTransitionsevent.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                #region 後台
                cfg.CreateMap<Exchange, Areas.Admin.ViewModels.Exchange.ExchangeView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Exchange.ExchangeView, Exchange>();
                #endregion
                cfg.CreateMap<Exchange, ViewModels.Exchange.ExchangeView>();
                cfg.CreateMap<ViewModels.Exchange.ExchangeView, Exchange>();
            });
        }
    }
}